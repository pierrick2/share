package com.share.test.core.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.share.core.dao.UserDao
import com.share.core.dto.Token
import com.share.core.properties.LoggerProperties
import com.share.core.properties.SecurityProperties
import com.share.core.service.TokenService
import com.share.core.service.impl.TokenServiceImpl
import com.share.test.core.authenticationRegistryFrRememberFalse
import com.share.test.core.authenticationRegistryFrRememberTrue
import com.share.test.core.userConfirmFRTrue
import com.share.test.core.uuidToken
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.cache.CacheManager
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date
import java.util.Optional
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertSame

class TokenServiceTest {
    private val userDao: UserDao = mock()
    private val cacheManager: CacheManager = mock()
    private val keySecret = uuidToken.toString().toByteArray()
    private val tokenService: TokenService = TokenServiceImpl(SecurityProperties(uuidToken.toString()), userDao, cacheManager, LoggerProperties())
    private var initDate = false
    private var tokenJwts: Token = Token(UUID.randomUUID(), "")
        get() {
            if(field.tokenJwt.isEmpty()) {
                field = tokenService.createToken(authenticationRegistryFrRememberTrue)
                field.user = userConfirmFRTrue
            }
            return field
        }

    private var dateMonthExpiration: LocalDateTime = LocalDateTime.now()
        get() {
            initDateExpiration()
            return field
        }
    private var dateDayExpiration: LocalDateTime = LocalDateTime.now()
        get(){
            initDateExpiration()
            return field
        }

    private fun initDateExpiration() {
        if(!initDate) {
            val dateNow = LocalDateTime.now()
            val dateAtMidnight = LocalDateTime.of(dateNow.year, dateNow.month, dateNow.dayOfMonth, 0, 0, 0, 0)
            dateDayExpiration = dateAtMidnight.plusDays(1)
            dateMonthExpiration = dateAtMidnight.plusMonths(1)
            initDate = false
        }
    }

    @Test
    fun testTokenIdentifyOfCreateToken() {
        // given

        // when
        val token = tokenService.createToken(authenticationRegistryFrRememberTrue)

        // then
        val claims = Jwts.parser().setSigningKey(keySecret).parseClaimsJws(token.tokenJwt).body
        assertEquals(token.tokenIdentify.toString(), claims.subject)
    }

    @Test
    fun testDateExpirationOfCreateTokenWhenRemember() {
        // given

        // when
        val token = tokenService.createToken(authenticationRegistryFrRememberTrue)

        // then
        val claims = Jwts.parser().setSigningKey(keySecret).parseClaimsJws(token.tokenJwt).body
        assertEquals(dateMonthExpiration.atZone(ZoneId.systemDefault()).toInstant(), claims.expiration.toInstant())
    }

    @Test
    fun testDateExpirationOfCreateTokenWhenNotRemember() {
        // given

        // when
        val token = tokenService.createToken(authenticationRegistryFrRememberFalse)

        // then
        val claims = Jwts.parser().setSigningKey(keySecret).parseClaimsJws(token.tokenJwt).body
        assertEquals(dateDayExpiration.atZone(ZoneId.systemDefault()).toInstant(), claims.expiration.toInstant())
    }

    @Test
    fun testFindUSerByTokenWhenUserExist() {
        // given
        whenever(userDao.findByPseudoAndToken(any(), any())).thenReturn(Optional.of(userConfirmFRTrue))

        // when
        val token = tokenService.findUserByToken(tokenJwts.tokenJwt)

        //then
        assertNotNull(token)
        assertEquals(token.tokenIdentify, tokenJwts.tokenIdentify)
        assertEquals(token.tokenJwt, tokenJwts.tokenJwt)
        assertSame(token.user, tokenJwts.user)
    }

    @Test
    fun testFindUSerByTokenWhenUserNotExist() {
        // given
        whenever(userDao.findByPseudoAndToken(any(), any())).thenReturn(Optional.empty())

        // when
        val token = tokenService.findUserByToken(tokenJwts.tokenJwt)

        //then
        assertNull(token)
    }

    @Test
    fun testFindUSerByTokenWhenTokenExpired() {
        // given
        val tokenExpired = Jwts.builder().setId(userConfirmFRTrue.pseudo).setSubject(uuidToken.toString())
                .setExpiration(Date.from(LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, keySecret).compact()
        // when
        val token = tokenService.findUserByToken(tokenExpired)

        //then
        assertNull(token)
    }
}