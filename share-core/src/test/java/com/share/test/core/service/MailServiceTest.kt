package com.share.test.core.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.whenever
import com.share.core.enums.ErrorCode
import com.share.core.enums.Nationality
import com.share.core.exception.AuthenticationException
import com.share.test.core.authenticationRegistryFrRememberTrue
import com.share.test.core.bodyMailConfirmEnTrue
import com.share.test.core.bodyMailConfirmFrTrue
import com.share.test.core.causeException
import com.share.test.core.subjectConfirmEn
import com.share.test.core.subjectConfirmFr
import com.share.test.core.subjectRegistry
import com.share.test.core.textMailConfirmEnTrue
import com.share.test.core.textMailConfirmFr
import com.share.test.core.textMailRegistryFr
import com.share.test.core.userConfirmENTrue
import com.share.test.core.userConfirmFRTrue
import com.share.test.core.uuidRegistry
import org.mockito.ArgumentMatchers
import org.springframework.mail.MailSendException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class MailServiceTest: TemplateMailServiceMock() {



    @Test
    fun testExceptionSendMail() {
        // given
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailRegistryFr)
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectRegistry)
        whenever(mailSender.send(any())).thenThrow(MailSendException("toto", causeException))
        // when
        try {
            mailService.sendMailRegistry(authenticationRegistryFrRememberTrue, uuidRegistry)
            fail()
        } catch (authenticationException: AuthenticationException) {
            // then
            assertEquals(authenticationException.code, ErrorCode.MAIL.code)
        }
    }

    @Test
    fun testSendMailConfirmFr(){
        // given
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailConfirmFr)
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectConfirmFr)
        whenever(mailSender.send(eq(bodyMailConfirmEnTrue))).then { fail() }
        // when
        mailService.sendMailConfirm(userConfirmFRTrue)
    }

    @Test
    fun testSendMailConfirmEn(){
        // given
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), any(), eq(Nationality.EN.locale))).thenReturn(textMailConfirmEnTrue)
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), eq(null), eq(Nationality.EN.locale))).thenReturn(subjectConfirmEn)
        whenever(mailSender.send(eq(bodyMailConfirmFrTrue))).then { fail() }
        // when
        mailService.sendMailConfirm(userConfirmENTrue)
    }
}