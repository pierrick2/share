package com.share.test.core.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.share.core.dao.AccessDao
import com.share.core.dao.TokenValidityActionDao
import com.share.core.dao.UserDao
import com.share.core.dto.Authentication
import com.share.core.dto.Token
import com.share.core.entity.model.Access
import com.share.core.entity.model.TokenValidityAction
import com.share.core.entity.model.User
import com.share.core.enums.AccessName
import com.share.core.enums.ErrorCode
import com.share.core.enums.Nationality
import com.share.core.exception.AuthenticationException
import com.share.core.properties.LoggerProperties
import com.share.core.properties.SecurityProperties
import com.share.core.service.AuthenticationService
import com.share.core.service.TokenService
import com.share.core.service.impl.AuthenticationServiceImpl
import com.share.core.service.impl.TokenServiceImpl
import com.share.test.core.authenticationRegistryFrRememberTrue
import com.share.test.core.subjectConfirmFr
import com.share.test.core.subjectRegistry
import com.share.test.core.textMailConfirmFr
import com.share.test.core.textMailRegistryFr
import com.share.test.core.userConfirmFRTrue
import com.share.test.core.uuidRegistry
import com.share.test.core.uuidToken
import org.mockito.ArgumentMatchers
import org.springframework.cache.CacheManager
import java.util.Optional
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertSame
import kotlin.test.fail

class AuthenticationServiceTest: TemplateMailServiceMock() {
    private val userDao: UserDao = mock()
    private val tokenValidityActionDao: TokenValidityActionDao = mock()
    private val cacheManager: CacheManager = mock()
    private val accessDao: AccessDao = mock()
    private val tokenService: TokenService = TokenServiceImpl(SecurityProperties(uuidToken.toString()), userDao, cacheManager, LoggerProperties())
    private val authenticationService: AuthenticationService = AuthenticationServiceImpl(mailService, userDao, accessDao, tokenValidityActionDao, tokenService, loggerProperties)


    @Test(expected = AuthenticationException::class)
    fun testRegistrywhenPseudoNull() {
        // given
        val authentication = Authentication(null, "testPasswordRegistryFrTrue","testMessageRegistryFrTrue", Nationality.FR,"testMailRegistryFrTrue",true)

        // when
        authenticationService.registry(authentication)
    }

    @Test(expected = AuthenticationException::class)
    fun testRegistrywhenMessageNull() {
        // given
        val authentication = Authentication("testPseudoRegistryFrTrue", "testPasswordRegistryFrTrue",null, Nationality.FR,"testMailRegistryFrTrue",true)

        // when
        authenticationService.registry(authentication)
    }

    @Test(expected = AuthenticationException::class)
    fun testRegistrywhenPasswordNull() {
        // given
        val authentication = Authentication("testPseudoRegistryFrTrue", null,"testMessageRegistryFrTrue", Nationality.FR,"testMailRegistryFrTrue",true)

        // when
        authenticationService.registry(authentication)
    }

    @Test(expected = AuthenticationException::class)
    fun testRegistrywhenNationalityNull() {
        // given
        val authentication = Authentication("testPseudoRegistryFrTrue", "testPasswordRegistryFrTrue","testMessageRegistryFrTrue", null,"testMailRegistryFrTrue",true)

        // when
        authenticationService.registry(authentication)
    }

    @Test(expected = AuthenticationException::class)
    fun testRegistrywhenMailNull() {
        // given
        val authentication = Authentication("testPseudoRegistryFrTrue", "testPasswordRegistryFrTrue","testMessageRegistryFrTrue", Nationality.FR,null,true)

        // when
        try {
            authenticationService.registry(authentication)
            fail()
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.EMPTY_PROPERTY.code)
            throw authenticationException
        }
    }

    @Test(expected = AuthenticationException::class)
    fun testRegistryWhenUserNotFound() {
        //given
        whenever(userDao.findByPseudo(any())).thenReturn(Optional.of(userConfirmFRTrue))

        // when
        try {
            authenticationService.registry(authenticationRegistryFrRememberTrue)
            fail()
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.REGISTRY.code)
            throw authenticationException
        }
    }

    @Test
    fun testRegistry() {
        //given
        whenever(userDao.findByPseudo(any())).thenReturn(Optional.empty())
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailRegistryFr)
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectRegistry)
        whenever(tokenValidityActionDao.save(ArgumentMatchers.any(TokenValidityAction::class.java))).thenReturn(TokenValidityAction())
        whenever(userDao.save(ArgumentMatchers.any(User::class.java))).thenReturn(User())
        whenever(accessDao.findByName(any())).thenReturn(Access())

        // when
        val token = authenticationService.registry(authenticationRegistryFrRememberTrue)

        // then
        assertNotNull(token)
    }

    @Test(expected = AuthenticationException::class)
    fun testConfirmRegistryWhenTokenValidityActionNoExist() {
        // given
        whenever(tokenValidityActionDao.findByValidityAndPseudoAndAction(any(), any(), any())).thenReturn(null)

        // when
        try {
            authenticationService.confirmRegistry(authenticationRegistryFrRememberTrue.pseudo!!, uuidRegistry)
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.CONFIRM.code)
            throw authenticationException
        }
    }

    @Test
    fun testConfirmRegistry() {
        // given
        whenever(tokenValidityActionDao.findByValidityAndPseudoAndAction(any(), any(), any())).thenReturn(TokenValidityAction(userConfirmFRTrue))
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailConfirmFr)
        whenever(messageSource.getMessage(ArgumentMatchers.anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectConfirmFr)
        whenever(userDao.save(ArgumentMatchers.any(User::class.java))).thenReturn(userConfirmFRTrue)

        // when
        authenticationService.confirmRegistry(authenticationRegistryFrRememberTrue.pseudo!!, uuidRegistry)
    }

    @Test(expected = AuthenticationException::class)
    fun testCheckCodeAndGiveFreeAccessWhenCodeWrong() {
        // when
        try {
            authenticationService.checkCodeAndGiveFreeAccess(Token(uuidToken, ""), 123)
            fail()
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.CODE.code)
            throw authenticationException
        }
    }

    @Test
    fun testCheckCodeAndGiveFreeAccess() {
        // given
        val user = User(code = 123)
        val accessUser = Access(AccessName.USER)
        whenever(accessDao.findByName(eq(AccessName.USER))).thenReturn(accessUser)
        whenever(userDao.save(ArgumentMatchers.any(User::class.java))).thenReturn(User())

        // when
        authenticationService.checkCodeAndGiveFreeAccess(Token(uuidToken, "",user), 123)

        // then
        assertSame(user.access, accessUser)
    }

    @Test(expected = AuthenticationException::class)
    fun testAuthenticateWhenPasswordNull() {
        // given
        val authentication = Authentication("testPseudoRegistryFrTrue", null,"testMessageRegistryFrTrue", Nationality.FR,null,true)

        // when
        try {
            authenticationService.authenticate(authentication)
            fail()
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.EMPTY_PROPERTY.code)
            throw authenticationException
        }
    }

    @Test(expected = AuthenticationException::class)
    fun testAuthenticateWhenPseudoNull() {
        // given
        val authentication = Authentication(null, "testPasswordRegistryFrTrue","testMessageRegistryFrTrue", Nationality.FR,null,true)

        // when
        authenticationService.authenticate(authentication)
    }

    @Test(expected = AuthenticationException::class)
    fun testAuthenticateWhenUserNotPresent() {
        // given
        whenever(userDao.findByPseudo(any())).thenReturn(Optional.empty())
        // when
        authenticationService.authenticate(authenticationRegistryFrRememberTrue)
    }

    @Test(expected = AuthenticationException::class)
    fun testAuthenticateWhenPasswordWrong() {
        // given
        whenever(userDao.findByPseudo(any())).thenReturn(Optional.of(User(password = "falsePassword")))
        // when
        try {
            authenticationService.authenticate(authenticationRegistryFrRememberTrue)
            fail()
        } catch (authenticationException: AuthenticationException) {
            assertEquals(authenticationException.code, ErrorCode.LOGIN.code)
            throw authenticationException
        }
    }

    @Test
    fun testAuthenticate() {
        // given
        whenever(userDao.findByPseudo(any())).thenReturn(Optional.of(userConfirmFRTrue))
        whenever(userDao.save(ArgumentMatchers.any(User::class.java))).thenReturn(User())
        // when
        authenticationService.authenticate(authenticationRegistryFrRememberTrue)
    }
}