package com.share.test.core

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.share.core.enums.Nationality
import com.share.core.properties.TemplateMail
import org.mockito.ArgumentMatchers.anyString
import org.springframework.context.MessageSource
import kotlin.test.Test
import kotlin.test.assertEquals

class TemplateMailTest {
    private val messageSource: MessageSource = mock()
    private val templateMail = TemplateMail("http://localhost:8080/share/rest/authenticate", "http://localhost:8080/share/login"
    ,"pierrick.meignant@gmail.com", "", "", "", "", messageSource)


    @Test
    fun testBodyMailRegister() {
        // given
        whenever(messageSource.getMessage(anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailRegistryFr)
        whenever(messageSource.getMessage(anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectRegistry)
        // when
        val bodyMailRegister = templateMail.bodyMailRegistry(authenticationRegistryFrRememberTrue, uuidRegistry)
        // then
        assertEquals(bodyMailRegistryFrTrue, bodyMailRegister)
    }

    @Test
    fun testBodyMailConfirmFr() {
        // given
        whenever(messageSource.getMessage(anyString(), any(), eq(Nationality.FR.locale))).thenReturn(textMailConfirmFr)
        whenever(messageSource.getMessage(anyString(), eq(null), eq(Nationality.FR.locale))).thenReturn(subjectConfirmFr)
        // when
        val bodyMailRegister = templateMail.bodyMailConfirm(userConfirmFRTrue)
        // then
        assertEquals(bodyMailConfirmFrTrue, bodyMailRegister)
    }

    @Test
    fun testBodyMailConfirmEn() {
        // given
        whenever(messageSource.getMessage(anyString(), any(), eq(Nationality.EN.locale))).thenReturn(textMailConfirmEnTrue)
        whenever(messageSource.getMessage(anyString(), eq(null), eq(Nationality.EN.locale))).thenReturn(subjectConfirmEn)
        // when
        val bodyMailRegister = templateMail.bodyMailConfirm(userConfirmENTrue)
        // then
        assertEquals(bodyMailConfirmEnTrue, bodyMailRegister)
    }
}