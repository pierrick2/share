package com.share.test.core.service

import com.nhaarman.mockitokotlin2.mock
import com.share.core.properties.LoggerProperties
import com.share.core.properties.TemplateMail
import com.share.core.service.impl.MailServiceImpl
import org.springframework.context.MessageSource
import org.springframework.mail.MailSender

abstract class TemplateMailServiceMock {
    protected val messageSource: MessageSource = mock()
    protected val templateMail = TemplateMail("http://localhost:8080/share/rest/authenticate", "http://localhost:8080/share/login"
            ,"pierrick.meignant@gmail.com","", "", "", "", messageSource)

    protected val mailSender: MailSender = mock()
    protected val loggerProperties: LoggerProperties = LoggerProperties()
    protected val mailService = MailServiceImpl(templateMail, mailSender, loggerProperties)
}