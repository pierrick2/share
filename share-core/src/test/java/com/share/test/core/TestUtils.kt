package com.share.test.core

import com.share.core.dto.Authentication
import com.share.core.entity.model.Access
import com.share.core.entity.model.User
import com.share.core.enums.AccessName
import com.share.core.enums.Nationality
import org.springframework.mail.SimpleMailMessage
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.UUID

val authenticationRegistryFrRememberFalse = Authentication("testRegistryFrFalse", "testPasswordRegistryFrFalse","testMessageRegistryFrFalse", Nationality.FR,"testMailRegistryFrFalse",false)
val authenticationRegistryFrRememberTrue = Authentication("testRegistryFrTrue", "testPasswordRegistryFrTrue","testMessageRegistryFrTrue", Nationality.FR,"testMailRegistryFrTrue",true)
val authenticationRegistryEnRememberFalse = Authentication("testRegistryEnFalse", "testPasswordRegistryEnFalse","testMessageRegistryEnFalse", Nationality.EN,"testMailRegistryEnFalse",false)
val authenticationRegistryEnRememberTrue = Authentication("testRegistryEnTrue", "testPasswordRegistryEnTrue","testMessageRegistryEnTrue", Nationality.EN,"testMailRegistryEnTrue",true)

val causeException = Exception("causeException")
    get() {
        if(field.stackTrace.isEmpty()) {
            field.stackTrace[0] = StackTraceElement("classFrst", "methodClassParent", "fileException", 10)
        }
        return field
    }
val uuidRegistry = UUID.randomUUID()
val uuidToken = UUID.randomUUID()
val codeConfirm = 12345

val userConfirmFRTrue = User("testRegistryFrTrue",  BCryptPasswordEncoder(11).encode(authenticationRegistryFrRememberTrue.password), Access(AccessName.GUEST), uuidToken, "testMailRegistryFrTrue", Nationality.FR, codeConfirm)
val userConfirmENTrue = User("testRegistryEnTrue",  BCryptPasswordEncoder(11).encode(authenticationRegistryEnRememberTrue.password), Access(AccessName.GUEST), uuidToken, "testMailRegistryEnTrue", Nationality.EN, codeConfirm)

val subjectRegistry = "SHARE - Candidature"
val subjectConfirmFr = "SHARE - Candidature acceptée"
val subjectConfirmEn = "SHARE - Registry confirm"

val textMailRegistryFr = "Bonjour,\n\nUn nouvel utilisateur ${"testRegistryFrTrue".toUpperCase()} souhaite se connecter à share.\nson message: testMessageRegistryFrTrue\n\n\nhttp://localhost:8080/transaction/rest/authenticate/testPseudoRegistryFrTrue/$uuidRegistry"
val textMailConfirmFr = "Bonjour testRegistryFrTrue,\\n\\nVotre demande a été accepté.e, vos accès seront déverouillez quand vous mettrez ce code: $codeConfirm\\n\\nurl de la page de login: http://localhost:8080/share/login"
val textMailConfirmEnTrue = "Hello testRegistryEnTrue,\\n\\nYour request has been accepting, Your access will be activate when you enter this code : $codeConfirm\\n\\nurl to connect at share: http://localhost:8080/share/login"

val bodyMailRegistryFrTrue = SimpleMailMessage()
    get() {
        if(field.subject == null) {
            field.setTo("pierrick.meignant@gmail.com")
            field.subject = subjectRegistry
            field.text = textMailRegistryFr
        }
        return field
    }

val bodyMailConfirmFrTrue = SimpleMailMessage()
    get() {
        if(field.subject == null) {
            field.setTo("testMailRegistryFrTrue")
            field.subject = subjectConfirmFr
            field.text = textMailConfirmFr
        }
        return field
    }

val bodyMailConfirmEnTrue = SimpleMailMessage()
    get() {
        if(field.subject == null) {
            field.setTo("testMailRegistryEnTrue")
            field.subject = subjectConfirmEn
            field.text = textMailConfirmEnTrue
        }
        return field
    }