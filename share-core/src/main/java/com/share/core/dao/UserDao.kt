package com.share.core.dao

import com.share.core.entity.model.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional
import java.util.UUID

/**
 * @author pierrick meignant
 */
interface UserDao: JpaRepository<User, Long> {
    fun findByPseudo(pseudo: String): Optional<User>
    fun findByPseudoAndToken(pseudo: String, token: UUID): Optional<User>
}