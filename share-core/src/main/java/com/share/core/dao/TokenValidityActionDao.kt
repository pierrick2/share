package com.share.core.dao

import com.share.core.entity.model.TokenValidityAction
import com.share.core.enums.Action
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.UUID

/**
 * @author pierrick meignant
 */
interface TokenValidityActionDao: JpaRepository<TokenValidityAction, UUID> {

    @Query("SELECT tokenValidityAction FROM TokenValidityAction tokenValidityAction " +
            "WHERE tokenValidityAction.validity = :validity AND tokenValidityAction.user.pseudo = :pseudo AND tokenValidityAction.action = :action")
    fun findByValidityAndPseudoAndAction(@Param("validity")validity: UUID, @Param("pseudo") pseudo: String, @Param("action") action: Action): TokenValidityAction?

}