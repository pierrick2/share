package com.share.core.dao

import com.share.core.entity.model.Access
import com.share.core.enums.AccessName
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * @author pierrick meignant
 */
interface AccessDao: JpaRepository<Access, Long> {
    @Query("SELECT access FROM Access access WHERE access.name = :accessName")
    fun findByName(@Param("accessName") accessName: AccessName): Access
}