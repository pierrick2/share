package com.share.core.enums

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.Locale

/**
 * @author pierrick meignant
 */
@JsonFormat(shape = JsonFormat.Shape.STRING)
enum class Nationality {
    FR,EN;

    val locale: Locale get(){
        return when(this) {
           FR -> Locale.FRANCE
           EN -> Locale.ENGLISH
        }
    }
}