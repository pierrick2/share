package com.share.core.enums

import com.fasterxml.jackson.annotation.JsonFormat

/**
 * @author pierrick meignant
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class ErrorCode(val code: Int,
                     val message: String) {
    DEFAULT(-1, "Is default error, not implement"),
    LOGIN(401, "The user with login doesn't exist"),
    CODE(504, "code is not same"),
    SESSION(501, "The session is not initialize"),
    EMPTY_PROPERTY(402, "the form authentication is wrong"),
    CONFIRM(502, "the code is not valid"),
    REGISTRY(403, "The pseudo already exist"),
    MAIL(503, "Mail no send correctly");

    companion object {
        private val ERRORS: MutableMap<Int, ErrorCode> = mutableMapOf()
        private val ERRORS_BY_MESSAGE: MutableMap<String, ErrorCode> = mutableMapOf()
        private val ERRORS_BY_NAME: MutableMap<String, ErrorCode> = mutableMapOf()

        private fun initializeMap() {
            if(ERRORS.isEmpty()) {
                val values = values()
                for(value in values) {
                    ERRORS[value.code] = value
                    ERRORS_BY_MESSAGE[value.message.toLowerCase()] = value
                    ERRORS_BY_NAME[value.name] = value
                }
            }
        }

        fun valueOf(regex: Int): ErrorCode? {
            initializeMap()
            return ERRORS[regex]
        }

        fun valueOf(regex: String): ErrorCode? {
            initializeMap()
            return ERRORS_BY_NAME[regex.toUpperCase()] ?: ERRORS_BY_MESSAGE[regex.toLowerCase()]
        }
    }

    override fun toString(): String {
        return "EnumError(code=$code, message='$message')"
    }


}