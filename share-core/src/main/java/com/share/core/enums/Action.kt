package com.share.core.enums
/**
 * @author pierrick meignant
 */
enum class Action {
    REGISTRY, SUPPRESS, MODIFY_PASSWORD
}