package com.share.core.enums

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

/**
 * @author pierrick meignant
 */
@JsonFormat(shape = JsonFormat.Shape.STRING)
enum class AccessName() {
    GUEST,USER,ADMIN,SERVER;

    var accesses: List<GrantedAuthority> = listOf()
        get(){
            if(field.isEmpty()) {
                val accessesUser = values().filter { accessName -> accessName.ordinal <= ordinal }
                val accesses: MutableList<GrantedAuthority> = mutableListOf()
                for(access in accessesUser) {
                    accesses.add(SimpleGrantedAuthority("ROLE_${access.name}"))
                }
                field = accesses
            }
            return field
        }
        private set

    init {

    }

}