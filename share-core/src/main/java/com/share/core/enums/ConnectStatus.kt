package com.share.core.enums

import com.fasterxml.jackson.annotation.JsonFormat

/**
 * @author pierrick meignant
 */
@JsonFormat(shape = JsonFormat.Shape.STRING)
enum class ConnectStatus {
    CONNECT, DISCONNECT
}