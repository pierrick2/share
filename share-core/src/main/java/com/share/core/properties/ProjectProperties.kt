package com.share.core.properties

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

/**
 * @author pierrick meignant
 */
@Component
@Scope(value="singleton")
class ProjectProperties(
        @Value("\${project.maven.version}")
        val version: String
) {
}