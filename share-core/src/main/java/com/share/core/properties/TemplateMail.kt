package com.share.core.properties

import com.share.core.dto.Authentication
import com.share.core.entity.model.User
import com.share.core.enums.Nationality
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Scope
import org.springframework.mail.SimpleMailMessage
import org.springframework.stereotype.Component
import java.util.UUID

/**
 * @author pierrick meignant
 */
@Component
@Scope("singleton")
class TemplateMail(
        @Value("\${url.registry}")
        private val urlRegistry: String,
        @Value("\${url.confirm}")
        private val urlConfirm: String,
        @Value("\${mail.receive}")
        private val receive: String,
        @Value("mail.body.registry")
        private val bodyRegistry: String,
        @Value("mail.body.confirm")
        private val bodyConfirm: String,
        @Value("mail.subject.register")
        private val subjectRegistry: String,
        @Value("mail.subject.confirm")
        private val subjectConfirm: String,
        private val messageSource: MessageSource
) {
    private fun translate(keyMessage: String, nationality: Nationality, arguments: Array<Any>? = null): String {
        return messageSource.getMessage(keyMessage, arguments, nationality.locale)
    }

    private fun bodySignupWithArguments(authentication: Authentication, uuid: UUID)
            = translate(bodyRegistry, authentication.nationality!!,
            arrayOf(authentication.pseudo!!.toUpperCase(), authentication.message!!,urlRegistry, authentication.pseudo!!, uuid))


    private fun bodyConfirmWithArgument(user: User)
            =  translate(bodyConfirm, user.nationality, arrayOf(user.pseudo, user.code!!, urlConfirm))

    /**
     * @param subject subject of mail
     * @param receiver the target receive mail
     * @param nationality the language translate mail
     * @param body the message in mail
     * @return object mail to send at user
     */
    private fun createBodyMail(subject: String, receiver: String, nationality: Nationality, body: String): SimpleMailMessage {
        val mailMessage = SimpleMailMessage()
        mailMessage.setTo(receiver)
        mailMessage.subject = translate(subject, nationality)
        mailMessage.text = body
        return mailMessage
    }

    fun bodyMailRegistry(authentication: Authentication, uuid: UUID) =
            createBodyMail(subjectRegistry, receive, Nationality.FR, bodySignupWithArguments(authentication, uuid))

    fun bodyMailConfirm(user: User) =
            createBodyMail(subjectConfirm, user.mail!!, user.nationality, bodyConfirmWithArgument(user))

}