package com.share.core.properties

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

/**
 * @author pierrick meignant
 */
@Component
@Scope("singleton")
class LoggerProperties(
        @Value("\${logger.start}")
        val start: String = "",
        @Value("\${logger.stop}")
        val stop: String = "",
        @Value("\${logger.service.authenticate.checkUser}")
        val checkUser: String = "",
        @Value("\${logger.service.authenticate.confirm}")
        val confirm: String = "",
        @Value("\${logger.service.authenticate}")
        val authenticate: String = "",
        @Value("\${logger.service.authenticate.code}")
        val code: String = "",
        @Value("\${logger.service.authenticate.register}")
        val register: String = "",

        @Value("\${logger.mail.register}")
        val loggerMailRegister: String = "",
        @Value("\${logger.mail.confirm}")
        val loggerMailConfirm: String = "",
        @Value("\${logger.mail.error}")
        val loggerMailError: String = "",
        @Value("\${logger.controller.authenticate.registry}")
        val loggerRegistry: String = "",
        @Value("\${logger.controller.authenticate.addTokenInHeader}")
        val loggerAddTokenInHeader: String = "",
        @Value("\${logger.controller.authenticate.confirmRegistry}")
        val loggerConfirmRegistry: String = "",
        @Value("\${logger.controller.authenticate.code}")
        val loggerCode: String = "",
        @Value("\${logger.controller.authenticate}")
        val loggerAuthenticate: String = ""
)