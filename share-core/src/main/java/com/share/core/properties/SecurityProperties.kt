package com.share.core.properties

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

/**
 * @author pierrick meignant
 */
@Component
@Scope(value="singleton")
class SecurityProperties(
        @Value("\${jwt.secret.key}")
        val keySecret: String = "",
        @Value("\${header.access}")
        val access: String = "",
        @Value("\${header.authorize}")
        val authorize: String = "",
        @Value("header.bearer")
        val bearer: String = "",

        @Value("\${logger.service.token.createToken}")
        val loggerCreateToken: String = "",
        @Value("\${logger.service.token.findUserByToken}")
        val loggerFindUserByToken: String = "",
        @Value("\${logger.service.token.removeTokensExpired}")
        val loggerRemoveTokensExpired: String = "",
        @Value("\${logger.service.token.disconnect}")
        val loggerDisconnect: String = ""
) {
}