package com.share.core.properties

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

/**
 * @author pierrick meignant
 */
@Component
@Scope(value = "singleton")
class SwaggerProperties{
        @Value("\${contact.name}")
        lateinit var name: String
        @Value("\${contact.url}")
        lateinit var url: String
        @Value("\${contact.mail}")
        lateinit var mail: String
        @Value("\${apiInfo.title}")
        lateinit var title: String
        @Value("\${apiInfo.description}")
        lateinit var description: String
        @Value("\${docket.swaggerOn}")
        var swaggerOn: Boolean = false
}