package com.share.core.dto

import com.share.core.entity.model.User
import java.util.UUID

class Token(
        var tokenIdentify: UUID,
        var tokenJwt: String,
        var user: User = User()
)