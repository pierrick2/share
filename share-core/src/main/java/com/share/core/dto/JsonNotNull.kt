package com.share.core.dto

import com.fasterxml.jackson.annotation.JsonInclude

/**
 * @author pierrick meignant
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
abstract class JsonNotNull protected constructor()