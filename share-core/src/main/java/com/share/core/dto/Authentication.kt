package com.share.core.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.share.core.enums.Nationality

/**
 * @author pierrick meignant
 */
class Authentication(
        @field:JsonProperty("pseudo")
        var pseudo: String? = null,
        @field:JsonProperty("password", access = JsonProperty.Access.WRITE_ONLY)
        var password: String? = null,
        @field:JsonProperty("message")
        var message: String? = null,
        @field:JsonProperty("nationality")
        var nationality: Nationality? = null,
        @field:JsonProperty("mail")
        var mail: String? = null,
        @field:JsonProperty("remember")
        var remember: Boolean? = null
): JsonNotNull()