package com.share.core.dto.error

import com.fasterxml.jackson.annotation.JsonProperty
import com.share.core.dto.JsonNotNull
import com.share.core.enums.ErrorCode

/**
 * @author pierrick meignant
 */
class ErrorResponseApi(
        val errorCode: ErrorCode = ErrorCode.DEFAULT,
        @field:JsonProperty("object")
        val obj: Any? = null
): JsonNotNull(){

    override fun toString(): String {
        return "DTOError(error=$errorCode, obj=$obj)"
    }
}