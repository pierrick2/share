package com.share.core.exception

import com.share.core.enums.ErrorCode
import kotlin.reflect.KClass

/**
 * @author pierrick meignant
 */
class AuthenticationException private constructor(errorCode: ErrorCode, exceptionCall: KClass<*>,
                              obj: Any? = null, cause: Throwable? = null, 
                              enableSuppression: Boolean = true, writableStackTrace: Boolean = true
): ShareException(errorCode, exceptionCall, obj, cause, enableSuppression, writableStackTrace) {
    companion object{
        private val exceptionCall = AuthenticationException::class
        private val LOGIN = AuthenticationException(ErrorCode.LOGIN, exceptionCall)
        private val CODE = AuthenticationException(ErrorCode.CODE, exceptionCall)
        private val EMPTY_PROPERTY = AuthenticationException(ErrorCode.EMPTY_PROPERTY, exceptionCall)
        private val CONFIRM = AuthenticationException(ErrorCode.CONFIRM, exceptionCall)
        private val REGISTRY = AuthenticationException(ErrorCode.REGISTRY, exceptionCall)
        private val MAIL = AuthenticationException(ErrorCode.MAIL, exceptionCall)

        @Throws(AuthenticationException::class)
        fun LOGIN() {
            LOGIN.error
        }

        @Throws(AuthenticationException::class)
        fun MAIL(exception: Throwable) {
            MAIL.addException(exception)
        }

        @Throws(AuthenticationException::class)
        fun REGISTRY() {
            REGISTRY.error
        }

        @Throws(AuthenticationException::class)
        fun CONFIRM() {
            CONFIRM.error
        }

        @Throws(AuthenticationException::class)
        fun CODE() {
            CODE.error
        }

        @Throws(AuthenticationException::class)
        fun EMPTY_PROPERTY() {
            EMPTY_PROPERTY.warn
        }


    }
}