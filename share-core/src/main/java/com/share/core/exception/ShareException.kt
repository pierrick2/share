package com.share.core.exception

import com.share.core.dto.error.ErrorResponseApi
import com.share.core.enums.ErrorCode
import fr.libraries.core.utils.createLogger
import org.slf4j.Logger
import kotlin.reflect.KClass

/**
 * @author pierrick meignant
 */
abstract class ShareException(val errorCode: ErrorCode,
                              exceptionCall: KClass<*>,
                              var `object`: Any? = null,
                              cause: Throwable? = null, enableSuppression:
                               Boolean = true, writableStackTrace:
                               Boolean = true):
        Exception(errorCode.message, cause, enableSuppression, writableStackTrace) {

    private val logger: Logger = createLogger(exceptionCall)
    protected var exceptionCalled: Throwable? = null

    companion object{
        @JvmStatic
        fun format(message: String?, vararg args: Any) = message?.let { String.format(it,args) }
        @JvmStatic
        fun concat(list: List<String>): String{
            val value: StringBuilder = StringBuilder("")
            for(li in list) {
                value.append(" ").append(li)
            }
            return value.toString()
        }


    }


    private fun throwMe(exception: ShareException) {
        throw exception
    }

    fun addException(exception: Throwable) {
        exceptionCalled = exception
        val stackTrace = this.stackTrace.toMutableList()
        stackTrace.addAll(exception.stackTrace)
        this.stackTrace = stackTrace.toTypedArray()
        error
    }

    val code: Int
        get() = errorCode.code

    protected val error: Unit
        get() {
        logger.error("ERROR - {}", message)
        throwMe(this)
    }


    protected val warn: Unit
        get() {
            logger.warn("WARNING - {}", message)
            throwMe(this)
        }

    val dto get() = ErrorResponseApi(errorCode, `object`)

}