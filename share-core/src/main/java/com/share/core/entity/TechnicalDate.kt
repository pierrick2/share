package com.share.core.entity

import java.time.LocalDateTime
import java.time.ZoneId
import javax.persistence.Column
import javax.persistence.MappedSuperclass

/**
 * @author pierrick meignant
 */
@MappedSuperclass
abstract class TechnicalDate(@Column(name = "CREATED_DATE", nullable = false)
                                  var created: LocalDateTime = LocalDateTime.now(ZoneId.systemDefault()),
                             @Column(name = "UPDATED_DATE")
                                  var updated: LocalDateTime? = null)