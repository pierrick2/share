package com.share.core.entity.model

import com.share.core.entity.IdTable
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name = "FOLDER_FILE",
indexes = [
    Index(name = "hash_folder_file_name", columnList = "FOLDER_FILE_NAME", unique = false),
    Index(name = "hash_folder_file_path", columnList = "FOLDER_FILE_PATH", unique = true)
])
class FolderFile(
        @Column(name = "FOLDER_FILE_NAME", nullable = false, unique = false, length = 50)
        var name: String = "",
        @Column(name = "FOLDER_FILE_PATH", nullable = false, unique = true, length = 250)
        var path: String = "",
        @ManyToOne
        @JoinColumn(name = "FOLDER_FILE_OWNER", nullable = false, unique = false, referencedColumnName = "id")
        var owner: User = User(),
        @ManyToOne
        @JoinColumn(name = "FOLDER_FILE_PARENT", unique = true, referencedColumnName = "id")
        var parent: FolderFile? = null,
        @Column(name = "FOLDER_FILE_IS_FOLDER", nullable = false)
        var isFolder: Boolean = true,
        @Column(name = "FOLDER_FILE_BLOB")
        var file: ByteArray? = null
): IdTable() {
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    var children: List<FolderFile> = listOf()
    @OneToMany(mappedBy = "shareable.folderFile", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    var shares: List<Share> = listOf()
}