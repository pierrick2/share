package com.share.core.entity.model

import com.share.core.entity.IdTable
import com.share.core.enums.AccessName
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Index
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name = "ACCESS", indexes = [
    Index(name = "hash_acces_name", columnList = "ACCESS_NAME")
]
)
class Access(
        accessName: AccessName = AccessName.GUEST
): IdTable() {

    @Column(name = "ACCESS_NAME", nullable = false, length = 6, unique = true)
    @Enumerated(EnumType.STRING)
    var name: AccessName = accessName
        set(value){
            field = value
            order = value
        }
    @Column(name = "ACCESS_ORDER", unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    var order: AccessName = accessName
        private set
    @OneToMany(mappedBy = "access", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    val users: List<User> = listOf()
}