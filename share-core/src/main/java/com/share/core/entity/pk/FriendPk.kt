package com.share.core.entity.pk

import com.share.core.entity.model.User
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

/**
 * @author pierrick meignant
 */
@Embeddable
class FriendPk(
        @ManyToOne
    @JoinColumn(name = "FRIEND_USER_CONNECT", nullable = false)
    var userConnect: User = User(),
        @ManyToOne
    @JoinColumn(name = "FRIEND_USER", nullable = false)
    var friend: User = User()
): Serializable {
    override fun toString(): String {
        return "EntityFriendPk(userConnect=$userConnect, friend=$friend)"
    }
}