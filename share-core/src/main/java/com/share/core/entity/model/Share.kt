package com.share.core.entity.model

import com.share.core.entity.TechnicalDate
import com.share.core.entity.pk.SharePk
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name = "SHARE")
class Share(
        @EmbeddedId
        var shareable: SharePk = SharePk()
): TechnicalDate() {
}