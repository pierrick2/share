package com.share.core.entity.model

import com.share.core.entity.IdTable
import com.share.core.enums.Action
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name = "VALIDATION_ACTION",
indexes = [
    Index(name = "hash_validity_login", columnList = "VALIDITY_USER_LOGIN", unique = true),
    Index(name = "hash_validity_action", columnList = "ACTION", unique = false)
])
class TokenValidityAction(
        @ManyToOne
        @JoinColumn(name = "USER", referencedColumnName = "pseudo")
        var user: User = User(),
        @Column(name = "ACTION", nullable = false)
        @Enumerated(EnumType.STRING)
        val action: Action = Action.REGISTRY,
        @Column(name = "VALIDITY", nullable = false, length = 36)
        val validity: UUID = UUID.randomUUID()

): IdTable()