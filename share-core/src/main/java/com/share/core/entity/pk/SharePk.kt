package com.share.core.entity.pk

import com.share.core.entity.model.FolderFile
import com.share.core.entity.model.User
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

/**
 * @author pierrick meignant
 */
@Embeddable
class SharePk(
        @ManyToOne
        @JoinColumn(name = "USER_ID", referencedColumnName = "id", nullable = false)
        var user: User = User(),
        @ManyToOne
        @JoinColumn(name = "FILE_ID", referencedColumnName = "id", nullable = false)
        var folderFile: FolderFile = FolderFile()
): Serializable {
        override fun toString(): String {
                return "EntitySharePk(user=$user, folderFile=$folderFile)"
        }
}