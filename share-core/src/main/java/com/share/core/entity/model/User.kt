package com.share.core.entity.model

import com.share.core.entity.IdTable
import com.share.core.enums.Nationality
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name = "USER",
indexes = [
        Index(name = "hash_password_user", columnList = "USER_PASSWORD", unique = false),
        Index(name = "hash_token", columnList = "USER_TOKEN_IDENTIFY", unique = true),
        Index(name = "hash_pseudo", columnList = "USER_PSEUDO", unique = true)
])
class User(
        @Column(name = "USER_PSEUDO", nullable = false, unique = true, length = 20)
        var pseudo: String = "",
        @Column(name = "USER_PASSWORD", nullable = false, length = 30)
        var password: String = "",
        @ManyToOne
        @JoinColumn(name = "USER_ACCESS", referencedColumnName = "id", nullable = false)
        var access: Access = Access(),
        @Column(name = "USER_TOKEN_IDENTIFY", length = 36, unique = true)
        var token: UUID? = null,
        @Column(name = "USER_MAIL", length = 50)
        var mail: String? = null,
        @Column(name = "USER_NATIONALITY", length = 2)
        @Enumerated(EnumType.STRING)
        var nationality: Nationality = Nationality.FR,
        @Column(name = "USER_CODE", length = 5)
        var code: Int? = null
): IdTable() {

        @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
        var folderFiles: List<FolderFile> = listOf()

        @OneToMany(mappedBy = "shareable.user", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
        var shares: List<Share> = listOf()

        @OneToMany(mappedBy = "id.friend", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
        var friends: List<Friend> = listOf()

        @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
        var tokenValidityAction: List<TokenValidityAction> = listOf()
}