package com.share.core.entity.model

import com.share.core.entity.TechnicalDate
import com.share.core.entity.pk.FriendPk
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author pierrick meignant
 */
@Entity
@Table(name= "FRIEND")
class Friend(
    @EmbeddedId
    var id: FriendPk = FriendPk()
): TechnicalDate() {
}