package com.share.core.service

import com.share.core.properties.LoggerProperties
import fr.libraries.core.utils.createLogger
import org.slf4j.Logger

/**
 * @author pierrick meignant
 */
abstract class LoggerService protected constructor(
        protected val loggerProperties: LoggerProperties
) {
    protected val logger: Logger = createLogger(this)
    private fun info(logService: String, argumentOne: Any? = null, argumentTwo: Any? = null){
        if(argumentOne != null) {
            if(argumentTwo != null) {
                logger.info(logService, argumentOne, argumentTwo)
            } else {
                logger.info(logService, argumentOne)
            }

        } else {
            logger.info(logService)
        }
    }

    protected fun loggerStart(logService: String, argumentOne: Any? = null, argumentTwo: Any? = null) {
        info(loggerProperties.start + logService, argumentOne, argumentTwo)
    }

    protected fun loggerStop(logService: String,  argumentOne: Any? = null, argumentTwo: Any? = null) {
        info(loggerProperties.stop + logService, argumentOne, argumentTwo)
    }

    protected fun <T> loggerStop(result: T, logService: String,  argumentOne: Any? = null, argumentTwo: Any? = null): T {
        loggerStop(logService, argumentOne, argumentTwo)
        return result
    }
}