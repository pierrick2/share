package com.share.core.service.impl

import com.share.core.dto.Authentication
import com.share.core.entity.model.User
import com.share.core.exception.AuthenticationException
import com.share.core.properties.LoggerProperties
import com.share.core.properties.TemplateMail
import com.share.core.service.LoggerService
import com.share.core.service.MailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.MailException
import org.springframework.mail.MailSender
import org.springframework.mail.SimpleMailMessage
import org.springframework.stereotype.Service
import java.util.UUID

/**
 * @author pierrick meignant
 */
@Service
class MailServiceImpl @Autowired constructor(
        private val  templateMail: TemplateMail,
        private val mailSender: MailSender,
        loggerProperties: LoggerProperties
): LoggerService(loggerProperties), MailService {

    private fun sendMail(message: SimpleMailMessage) {
        try {
            mailSender.send(message)
        } catch (mailException: MailException) {
            //logger.error(loggerProperties.loggerMailError, mailException)
            AuthenticationException.MAIL(mailException)
        }
    }

    override fun sendMailRegistry(authentication: Authentication, uuid: UUID) {
        loggerStart(loggerProperties.loggerMailRegister)
        sendMail(templateMail.bodyMailRegistry(authentication, uuid))
        loggerStop(loggerProperties.loggerMailRegister)
    }

    override fun sendMailConfirm(user: User) {
        loggerStart(loggerProperties.loggerMailConfirm)
        sendMail(templateMail.bodyMailConfirm(user))
        loggerStop(loggerProperties.loggerMailConfirm)
    }

}