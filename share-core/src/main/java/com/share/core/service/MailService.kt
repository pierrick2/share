package com.share.core.service

import com.share.core.dto.Authentication
import com.share.core.entity.model.User
import java.util.UUID

/**
 * @author pierrick meignant
 */
interface MailService {
    /**
     * send mail when user registry
     * @param authentication it's context authenticate for registry
     */
    fun sendMailRegistry(authentication: Authentication, uuid: UUID)

    /**
     * send mail with code at user to confirm registry
     * @param user the user will be confirm registry
     */
    fun sendMailConfirm(user: User)
}