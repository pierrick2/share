package com.share.core.service.impl


import com.share.core.createCode
import com.share.core.dao.AccessDao
import com.share.core.dao.TokenValidityActionDao
import com.share.core.dao.UserDao
import com.share.core.dto.Authentication
import com.share.core.dto.Token
import com.share.core.entity.model.TokenValidityAction
import com.share.core.entity.model.User
import com.share.core.enums.AccessName
import com.share.core.enums.Action
import com.share.core.exception.AuthenticationException
import com.share.core.properties.LoggerProperties
import com.share.core.service.AuthenticationService
import com.share.core.service.LoggerService
import com.share.core.service.MailService
import com.share.core.service.TokenService
import com.share.core.updateEntity
import fr.libraries.core.utils.isNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.UUID

/**
 * @author pierrick meignant
 */
@Service
class AuthenticationServiceImpl @Autowired constructor(
        private val mailService: MailService,
        private val userDao: UserDao,
        private val accessDao: AccessDao,
        private val tokenValidityActionDao: TokenValidityActionDao,
        private val tokenService: TokenService,
        loggerProperties: LoggerProperties
): AuthenticationService, LoggerService(loggerProperties) {

    private val encryptor = BCryptPasswordEncoder(11)

    private fun createUserForRegistry(authentication: Authentication, tokenIdentify: UUID): User {
        val user = User(authentication.pseudo!!, encryptor.encode(authentication.password), accessDao.findByName(AccessName.GUEST),
                tokenIdentify, authentication.mail, authentication.nationality!!)
        return userDao.save(user)
    }

    override fun registry(authentication: Authentication): String {
        loggerStart(loggerProperties.register)
        if(!isNotNull(authentication.password, authentication.nationality, authentication.mail, authentication.pseudo, authentication.message)) {
            AuthenticationException.EMPTY_PROPERTY()
        }
        val pseudo = authentication.pseudo!!
        if(userDao.findByPseudo(pseudo).isPresent) {
            AuthenticationException.REGISTRY()
        }

        val token = tokenService.createToken(authentication)
        val user = createUserForRegistry(authentication, token.tokenIdentify)
        val tokenValidityAction = tokenValidityActionDao.save(TokenValidityAction( user, Action.REGISTRY))
        mailService.sendMailRegistry(authentication, tokenValidityAction.validity)
        return loggerStop(token.tokenJwt, logService =  loggerProperties.register)
    }

    override fun confirmRegistry(pseudo: String, uuid: UUID) {
        loggerStart(loggerProperties.confirm)
        val validate = tokenValidityActionDao.findByValidityAndPseudoAndAction(uuid, pseudo, Action.REGISTRY)
        if(validate == null) {
            AuthenticationException.CONFIRM()
        }
        with(validate!!.user) {
            code = createCode()
            updateEntity(this, userDao)
            mailService.sendMailConfirm(this)
        }

        loggerStop(loggerProperties.confirm)
    }

    override fun checkCodeAndGiveFreeAccess(tokenAuthenticate: Token, codeAuthentication: Int) {
        loggerStart(loggerProperties.code, codeAuthentication)
        val user = tokenAuthenticate.user
        if(user.code != codeAuthentication) {
            AuthenticationException.CODE()
        }
        user.mail = null
        user.code = null
        user.access = accessDao.findByName(AccessName.USER)
        updateEntity(user, userDao)
        tokenService.clearCacheToken(tokenAuthenticate.tokenJwt)
        loggerStop(loggerProperties.code, codeAuthentication)
    }

    override fun authenticate(authentication: Authentication): String {
        if(!isNotNull(authentication.password, authentication.pseudo)) {
            AuthenticationException.EMPTY_PROPERTY()
        }
        loggerStart(loggerProperties.authenticate, authentication.pseudo)
        val user = userDao.findByPseudo(authentication.pseudo!!)
        if(!user.isPresent || !encryptor.matches(authentication.password, user.get().password)) {
            AuthenticationException.LOGIN()
        }
        val token = tokenService.createToken(authentication)
        with(user.get()) {
            this.token = token.tokenIdentify
            updateEntity(this, userDao)
        }
        return loggerStop(token.tokenJwt, loggerProperties.authenticate, argumentOne = authentication.pseudo)
    }
}