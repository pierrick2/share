package com.share.core.service

import com.share.core.dto.Authentication
import com.share.core.dto.Token
import com.share.core.exception.AuthenticationException
import io.jsonwebtoken.JwtException
import java.util.UUID

/**
 * @author pierrick meignant
 */
interface AuthenticationService {
    /**
     * send mail at admin to confirm registry
     * @param authentication the context of registry
     * @return the token identify
     * @throws AuthenticationException if properties pseudo/password/nationality/mail empty or pseudo exist
     */
    @Throws(AuthenticationException::class)
    fun registry(authentication: Authentication): String

    /**
     * send mail at user with code to finalize registry (throw USER role)
     * @param pseudo user authenticate
     * @param uuid the token validity to confirm registry
     * @throws AuthenticationException if request to confirm not exist or pseudo is wrong
     */
    @Throws(AuthenticationException::class)
    fun confirmRegistry(pseudo: String, uuid: UUID)

    /**
     * if code is correct, so access guest become access user
     * @param tokenAuthenticate the tokenAuthenticate contain token and user
     * @param codeAuthentication the code authenticate
     * @throws AuthenticationException if code is not same, authentication not valid
     */
    @Throws(AuthenticationException::class, JwtException::class)
    fun checkCodeAndGiveFreeAccess(tokenAuthenticate: Token, codeAuthentication: Int)

    /**
     * authenticate user
     * @param authentication context authenticate (pseudo/password)
     * @return the token authenticate
     * @throws AuthenticationException if user not exist or properties pseudo/password empty
     */
    @Throws(AuthenticationException::class)
    fun authenticate(authentication: Authentication): String
}