package com.share.core.service

import com.share.core.dto.Authentication
import com.share.core.dto.Token
import io.jsonwebtoken.JwtException

interface TokenService {
    /**
     * create token when authenticate
     * @param authentication
     */
    fun createToken(authentication: Authentication): Token

    /**
     *find the user by token for authenticate (cacheable)
     * @param tokenJwts the token authenticate
     * @throws JwtException when token is bad (call with fail token)
     */
    @Throws(JwtException::class)
    fun findUserByToken(tokenJwts: String): Token?

    /**
     * clear cache at token authenticate for user
     * @param tokenJws the token authenticate at user
     */
    fun clearCacheToken(tokenJws: String): Boolean?
}