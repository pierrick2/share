package com.share.core.service.impl

import com.share.core.dao.UserDao
import com.share.core.dto.Authentication
import com.share.core.dto.Token
import com.share.core.properties.LoggerProperties
import com.share.core.properties.SecurityProperties
import com.share.core.service.LoggerService
import com.share.core.service.TokenService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date
import java.util.UUID

@Service
open class TokenServiceImpl @Autowired constructor(
        private val securityProperties: SecurityProperties,
        private val userDao: UserDao,
        private val cacheManager: CacheManager,
        loggerProperties: LoggerProperties
): LoggerService(loggerProperties), TokenService {

    private val keyJwt = securityProperties.keySecret.toByteArray()
    private val signatureAlgorithm = SignatureAlgorithm.HS512
    private lateinit var claims: Claims
    private val nameCache = "methodFindUserByToken"

    private fun createDateExpirationAtMidnight(remember: Boolean): LocalDateTime {
        val addOneMonthOrDayAtDate = 1L
        var date = LocalDateTime.now()
        date = LocalDateTime.of(date.year,date.month, date.dayOfMonth, 0,0,0,0)
        return if(remember) date.plusMonths(addOneMonthOrDayAtDate) else date.plusDays(addOneMonthOrDayAtDate)
    }

    override fun createToken(authentication: Authentication): Token {
        val pseudo = authentication.pseudo!!
        val remember = authentication.remember ?: false
        loggerStart(securityProperties.loggerCreateToken, pseudo, remember)
        val dateExpiration = createDateExpirationAtMidnight(remember)
        val tokenIdentify = UUID.randomUUID()
        val tokenJwts = Jwts.builder().setId(pseudo).setSubject(tokenIdentify.toString())
                .setExpiration(Date.from(dateExpiration.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(signatureAlgorithm, keyJwt).compact()
        return loggerStop(Token(tokenIdentify,tokenJwts), securityProperties.loggerCreateToken, pseudo, remember)
    }

    @Throws(ExpiredJwtException::class, UnsupportedJwtException::class, MalformedJwtException::class, SignatureException::class, IllegalArgumentException::class)
    private fun parseToken(tokenJwts: String): Claims {
        return Jwts.parser().setSigningKey(keyJwt).parseClaimsJws(tokenJwts).body
    }

    private fun isExpired(tokenJwts: String): Boolean {
        claims = try {
            parseToken(tokenJwts)
        } catch (e: ExpiredJwtException) {
            return true
        }
        return false
    }

    @Cacheable(value = ["methodFindUserByToken"], key = "tokenJwts")
    override fun findUserByToken(tokenJwts: String): Token? {
        loggerStart(securityProperties.loggerFindUserByToken, tokenJwts)
        return loggerStop(if(!isExpired(tokenJwts)) {
            val tokenIdentify = UUID.fromString(claims.subject)
            val user = userDao.findByPseudoAndToken(claims.id,tokenIdentify)
            if(user.isPresent) {
                Token(tokenIdentify, tokenJwts, user.get())
            } else {
                null
            }
        } else {
            null
        }, securityProperties.loggerFindUserByToken, tokenJwts)
    }

    override fun clearCacheToken(tokenJws: String) = cacheManager.getCache(nameCache)?.evictIfPresent(tokenJws)
}