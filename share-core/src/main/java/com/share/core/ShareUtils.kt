package com.share.core

import com.share.core.entity.TechnicalDate
import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDateTime

fun createCode() = ((Math.random() * 9999).toInt() + 1000)%100000

fun <T: TechnicalDate> updateEntity(entity: T, repository: JpaRepository<T, *>): T {
    entity.updated = LocalDateTime.now()
    return repository.save(entity)
}