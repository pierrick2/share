package com.share.middleware.rest

import com.share.core.dto.Authentication
import com.share.core.dto.Token
import com.share.core.properties.LoggerProperties
import com.share.core.properties.SecurityProperties
import com.share.core.service.AuthenticationService
import com.share.core.service.LoggerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.servlet.http.HttpServletResponse

/**
 * @author pierrick meignant
 */
@RestController
@RequestMapping("authenticate")
class AuthenticateController @Autowired constructor(
        private val authenticationService: AuthenticationService,
        private val securityProperties: SecurityProperties,
        loggerProperties: LoggerProperties
): LoggerService(loggerProperties) {

    private fun addTokenInHeader(response: HttpServletResponse, token: String) {
        logger.info(loggerProperties.loggerAddTokenInHeader)
        response.setHeader(securityProperties.access, securityProperties.authorize)
        response.setHeader(securityProperties.authorize, "${securityProperties.bearer}$token")
    }

    @PutMapping
    fun registry(@RequestBody authentication: Authentication, response: HttpServletResponse) {
        loggerStart(loggerProperties.loggerRegistry, authentication.pseudo)
        val token = authenticationService.registry(authentication)
        addTokenInHeader(response, token)
        loggerStop(loggerProperties.loggerRegistry, authentication.pseudo)
    }

    @GetMapping("{pseudo}/{tokenValidityRequest}")
    fun confirm(@PathVariable("pseudo") pseudo: String, @PathVariable("tokenValidityRequest")tokenValidityRequest: UUID) {
        loggerStart(loggerProperties.loggerConfirmRegistry, pseudo)
        authenticationService.confirmRegistry(pseudo, tokenValidityRequest)
        loggerStop(loggerProperties.loggerConfirmRegistry, pseudo)
    }

    @PostMapping("{codeAuthenticate}")
    fun codeToGiveFreeAccess(@AuthenticationPrincipal usernamePasswordAuthenticationToken: UsernamePasswordAuthenticationToken,
                             @PathVariable("codeAuthenticate") codeAuthenticate: Int) {
        val token: Token = usernamePasswordAuthenticationToken.principal as Token
        loggerStart(loggerProperties.loggerCode, token.user.pseudo)
        authenticationService.checkCodeAndGiveFreeAccess(token, codeAuthenticate)
        loggerStop(loggerProperties.loggerCode, token.user.pseudo)
    }

    @PostMapping
    fun authenticate(@RequestBody authentication: Authentication, response: HttpServletResponse) {
        loggerStart(loggerProperties.loggerAuthenticate, authentication.pseudo)
        val tokenJws = authenticationService.authenticate(authentication)
        addTokenInHeader(response, tokenJws)
        loggerStop(loggerProperties.loggerAuthenticate, authentication.pseudo)
    }
}