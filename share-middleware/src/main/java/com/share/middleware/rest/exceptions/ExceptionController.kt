package com.share.middleware.rest.exceptions

import com.share.core.dto.error.ErrorResponseApi
import com.share.core.enums.ErrorCode
import com.share.core.exception.AuthenticationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionController {
    @ExceptionHandler(RuntimeException::class)
    fun defaultError(): ResponseEntity<ErrorResponseApi> = ResponseEntity(ErrorResponseApi(ErrorCode.DEFAULT), HttpStatus.INTERNAL_SERVER_ERROR)

    @ExceptionHandler(AuthenticationException::class)
    fun authenticateError(authenticationException: AuthenticationException): ResponseEntity<ErrorResponseApi> {
        val httpStatus = when(authenticationException.errorCode) {
            ErrorCode.MAIL -> HttpStatus.NOT_MODIFIED
            ErrorCode.REGISTRY -> HttpStatus.FOUND
            else -> HttpStatus.BAD_REQUEST
        }
        return ResponseEntity(authenticationException.dto,httpStatus)
    }
}