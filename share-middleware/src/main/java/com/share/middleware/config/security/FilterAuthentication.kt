package com.share.middleware.config.security

import com.share.core.properties.SecurityProperties
import com.share.core.service.TokenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

@Component
class FilterAuthentication @Autowired constructor(
        private val securityProperties: SecurityProperties,
        private val tokenService: TokenService
): GenericFilterBean() {
    private val defaultTokenJws = "null"
    private fun convertToRequestAccesAtHeader(request: ServletRequest?): HttpServletRequest {
        if(request !is HttpServletRequest)   {
            throw RuntimeException("Expecting an HTTP request")
        }
        return request
    }

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, filterChain: FilterChain?) {
        val requestWithHeader = convertToRequestAccesAtHeader(request)
        var tokenJws = requestWithHeader.getHeader(securityProperties.authorize)
        if(tokenJws != null && defaultTokenJws != tokenJws) {
            tokenJws = tokenJws.replace(securityProperties.bearer, "")
            val tokenWithUser = tokenService.findUserByToken(tokenJws)
            if(tokenWithUser != null) {
                val restSecurityAuthenticate = UsernamePasswordAuthenticationToken(tokenWithUser, null, tokenWithUser.user.access.name.accesses)
                SecurityContextHolder.getContext().authentication = restSecurityAuthenticate
            }
        }
        filterChain?.doFilter(request, response)
    }

}