package com.share.middleware.config.logback

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.filter.Filter
import ch.qos.logback.core.spi.FilterReply

/**
 * @author pierrick meignant
 */
abstract class FilterLogback protected constructor(level: String,
                             haveToMessage: String,
                             private val accept: FilterReply,
                             private val deny: FilterReply): Filter<ILoggingEvent>()
{
    private val level = Level.valueOf(level).levelInt
    private val haveToMessage = haveToMessage.split(',').toList()

    private fun messageContains(message: String, haveToContain: Iterator<String>): Boolean {
        if(haveToContain.hasNext()) {
            return message.contains(haveToContain.next()) || messageContains(message, haveToContain)
        }
        return false
    }

    override fun decide(log: ILoggingEvent?): FilterReply {
        var result = FilterReply.DENY
        if (log != null && log.level.levelInt >= level) {
            result = if (messageContains(log.loggerName, haveToMessage.iterator())) accept else deny
        }
        return result
    }
}