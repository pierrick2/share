package com.share.middleware.config

import com.share.core.properties.ProjectProperties
import com.share.core.properties.SwaggerProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * @author pierrick meignant
 */
@Configuration
@EnableSwagger2
open class SwaggerConfig @Autowired constructor(
        private val swaggerProperties: SwaggerProperties,
        private val projectProperties: ProjectProperties
) {

   private val apiInfo get()  = ApiInfoBuilder().title(swaggerProperties.title).description(swaggerProperties.description)
            .version(projectProperties.version).contact(Contact(swaggerProperties.name, swaggerProperties.url,
                    swaggerProperties.mail)).build()

    @Bean
    open fun api(): Docket? {
            return Docket(DocumentationType.SWAGGER_2).enable(swaggerProperties.swaggerOn)
    }
}