package com.share.middleware.config.logback

import ch.qos.logback.core.spi.FilterReply

/**
 * @author pierrick meignant
 */
class FilterProject(): FilterLogback("INFO", "com.share.core,fr.libraries.explorer", FilterReply.ACCEPT, FilterReply.DENY) {
}