package com.share.middleware.config.logback

import ch.qos.logback.core.spi.FilterReply

/**
 * @author pierrick meignant
 */
class FilterOther(): FilterLogback("INFO", "com.share.core", FilterReply.DENY, FilterReply.ACCEPT) {
}